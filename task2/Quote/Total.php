<?php

namespace Quote;

/**
 * Class Total
 * @package Quote
 */
class Total
{
    protected $quote;

    /**
     * @var \Quote\Total\AbstractTotal[]
     */
    protected $totalObjects = [];

    /**
     * @var float
     */
    private $quoteAmount;

    /**
     * @var \Quote\Total\AbstractTotal[]
     */
    private $quoteTotals;

    /**
     * @var \Quote\Total\AbstractTotal[]
     */
    private $itemTotals;

    /**
     * @var \Quote\Total\AbstractTotal[]
     */
    private $quoteItemTotals;

    /**
     * Total constructor.
     */
    public function __construct(\Quote $quote)
    {
        $this->_registryTotal(\Quote\Total\Price::class);
        $this->_registryTotal(\Quote\Total\Discount::class);

        $this->quote = $quote;
        $this->_reset();
    }

    /**
     *
     */
    private function _registryTotal($class)
    {
        $this->totalObjects[] = new $class();
    }

    /**
     *
     */
    private function _reset()
    {
        $this->quoteAmount = 0.00;
        $this->itemTotals = [];
        $this->quoteTotals = [];
        $this->quoteItemTotals = [];
    }

    /**
     * @return $this
     */
    public function calculate()
    {
        $quoteItems = $this->quote->getItems();
        $this->_reset();

        foreach ($this->totalObjects as $total) {
            foreach ($quoteItems as $quoteItem) {
                $amount = $total->getAmount($quoteItem);
                if ($amount) {
                    /* quote */
                    if (!isset($this->quoteTotals[$total->getCode()])) {
                        $this->quoteTotals[$total->getCode()] = $total->getTotal();
                    }

                    /* items */
                    if (!isset($this->itemTotals[$quoteItem->getItemId()])) {
                        $this->itemTotals[$quoteItem->getItemId()] = [];
                    }
                    if (!isset($this->itemTotals[$quoteItem->getItemId()][$total->getCode()])) {
                        $this->itemTotals[$quoteItem->getItemId()][$total->getCode()] = $total->getTotal($quoteItem);
                    }

                    /* quote items */
                    if (!isset($this->quoteItemTotals[$quoteItem->getItemId()])) {
                        $this->quoteItemTotals[$quoteItem->getItemId()] = [
                            'code' => null,
                            'info' => null,
                            'total' => 0.00
                        ];
                    }

                    $this->quoteAmount += $amount;
                    $this->quoteTotals[$total->getCode()]['total'] += $amount;
                    $this->itemTotals[$quoteItem->getItemId()][$total->getCode()]['total'] += $amount;
                    $this->quoteItemTotals[$quoteItem->getItemId()]['total'] += $amount;
                }
            }
        }

        return $this;
    }

    /**
     * @param []
     */
    public function getQuoteTotals(\Quote $quote)
    {
        return $this->quoteTotals;
    }

    /**
     * @param \Quote $quote
     * @return float
     */
    public function getQuoteAmount()
    {
        return $this->quoteAmount;
    }

    /**
     * @param $item
     * @return array
     */
    public function getItemTotals($item)
    {
        return $this->itemTotals[$item->getItemId()];
    }

    /**
     * @param $item
     * @return float
     */
    public function getItemAmount($item)
    {
        /** @var \Quote\Total\AbstractTotal $total */
        return $this->quoteItemTotals[$item->getItemId()]['total'];
    }
}
