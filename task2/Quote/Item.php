<?php

namespace Quote;

/**
 * Class Item
 * @package Quote
 */
class Item
{
    /**
     * @var \Product
     */
    protected $product;

    /**
     * @var int
     */
    protected $qty;

    /**
     * @var int
     */
    protected $price;

    /**
     * @return string
     */
    public function getItemId()
    {
        return hash('md5', $this->getQty() . '#' . $this->getProduct()->getCode());
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price ?: 0.00;
    }

    /**
     * @param $qty
     */
    public function setQty($qty)
    {
        $this->qty = $qty;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param \Product $product
     */
    public function setProduct(\Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return \Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
