<?php

namespace Quote\Total;

/**
 * Class Discount
 * @package Quote\Total
 */
class Discount extends \Quote\Total\AbstractTotal
{
    /**
     * @return string
     */
    public function getCode()
    {
        return 'Discount';
    }

    /**
     * @param \Quote\Item $item
     * @return null
     */
    public function getInfo(\Quote\Item $item = null)
    {
        if ($item) {
            $productCode = $item->getProduct()->getCode();

            return $this->getDiscountInfo($productCode);
        }

        return 'discount';
    }

    /**
     * @param \Quote\Item $item
     * @return float
     */
    public function getAmount(\Quote\Item $item)
    {
        $productCode = $item->getProduct()->getCode();
        $discountQty = $this->getDiscountQty($productCode);
        if ($discountQty) {
            $discountPrice = $this->getDiscountPrice($productCode);

            $qty = $item->getQty();

            $fullDiscountParts = intval($qty / $discountQty);
            $fullDiscountAmmount = $discountPrice * $fullDiscountParts;

            return (float) -1 * $fullDiscountAmmount;
        }

        return 0.00;
    }

    /**
     * @param $productCode
     * @return null
     */
    protected function getDiscount($productCode, $attribute = 'price')
    {
        if (isset(\DataBase::$PRICERULES['discount'][$productCode][$attribute])) {
            return \DataBase::$PRICERULES['discount'][$productCode][$attribute];
        }

        return null;
    }

    /**
     * @param $productCode
     * @return float
     */
    public function getDiscountPrice($productCode)
    {
        $price = $this->getDiscount($productCode, 'price');
        return $price ?: 0.00;
    }

    /**
     * @param $productCode
     * @return int
     */
    public function getDiscountQty($productCode)
    {
        $qty = $this->getDiscount($productCode, 'qty');
        return $qty ?: 0;
    }

    /**
     * @param $productCode
     * @return string
     */
    public function getDiscountInfo($productCode)
    {
        $info = $this->getDiscount($productCode, 'info');
        return $info ?: '';
    }
}
