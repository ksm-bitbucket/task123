<?php

namespace Quote\Total;

/**
 * Class AbstractTotal
 * @package Quote\Total
 */
abstract class AbstractTotal
{
    /**
     * @return string
     */
    abstract public function getCode();

    /**
     * @param \Quote\Item $item
     * @return float
     */
    abstract public function getAmount(\Quote\Item $item);

    /**
     * @param \Quote\Item $item
     * @return string
     */
    abstract public function getInfo(\Quote\Item $item = null);

    /**
     * @param \Quote\Item $item
     * @return array
     */
    public function getTotal(\Quote\Item $item = null)
    {
        return [
            'code' => $this->getCode(),
            'info' => $this->getInfo($item),
            'total' => 0.00
        ];
    }

}
