<?php

namespace Quote\Total;

/**
 * Class Prise
 * @package Quote\Total
 */
class Price extends \Quote\Total\AbstractTotal
{
    /**
     * @return string
     */
    public function getCode()
    {
        return 'Price';
    }

    /**
     * @param \Quote\Item $item
     * @return null
     */
    public function getInfo(\Quote\Item $item = null)
    {
        return '';
    }

    /**
     * @param \Quote\Item $item
     * @return float
     */
    public function getAmount(\Quote\Item $item)
    {
        $qty = $item->getQty();

        return $qty * $item->getPrice();
    }
}
