<?php

/**
 * Class DataBase
 */
class DataBase
{
    public static $PRODUCTS = [
        'A' => [
            'code' => 'A',
            'name' => 'Apple',
            'price' => '2.00',
        ],
        'B' => [
            'code' => 'B',
            'name' => 'Berry',
            'price' => '12.00',
        ],
        'C' => [
            'code' => 'C',
            'name' => 'Carrot',
            'price' => '1.25',
        ],
        'D' => [
            'code' => 'D',
            'name' => 'Dried Super Sweet Snack',
            'price' => '0.15'
        ]
    ];

    public static $PRICERULES = [
        'discount' => [
            'A' => [
                'qty' => 4,
                'price' => '1.00', /* -1.00$ for 4 */
                'info' => '$2.00 each or 4 for $7.00'
            ],
            'C' => [
                'qty' => 6,
                'price' => '1.50', /* -1.50$ for 6 */
                'info' => '$6 for a six pack'
            ]
        ]
    ];
}
