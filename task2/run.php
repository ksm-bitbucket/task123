<?php

spl_autoload_register(function ($className) {
    include str_replace("\\", "/", $className . '.php');
});


$terminal = new \Terminal();


$terminal->newCustomer();
$terminal->scanLine('ABCDABAA');
$terminal->printCheck();


$terminal->newCustomer();
$terminal->scanLine('CCCCCCC');
$terminal->printCheck();


$terminal->newCustomer();
$terminal->scanCode('A');
$terminal->scanCode('B');
$terminal->scanLine('CD');
$terminal->printCheck();


$terminal->newCustomer();
$terminal->scanLine('AAAAAAAAAAABBCCCCCCCDDDDDD');
$terminal->printCheck();


$terminal->newCustomer();
$terminal->scanLine('');
$terminal->printCheck();
