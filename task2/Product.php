<?php
/**
 * Class Product
 */
class Product
{
    /**
     * @var
     */
    protected $code;
    protected $data;

    /**
     * @param $code
     * @return $this
     */
    public function loadByCode($code)
    {
        if (isset(\DataBase::$PRODUCTS[$code])) {
            $this->code = $code;
            $this->data = \DataBase::$PRODUCTS[$code];
        }

        return $this;
    }


    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return isset($this->data['code']) ? $this->data['code'] : null;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return isset($this->data['name']) ? $this->data['name'] : null;
    }

    /**
     * @return null
     */
    public function getPrice()
    {
        return isset($this->data['price']) ? $this->data['price'] : null;
    }
}
