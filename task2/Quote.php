<?php


/**
 * Class Quote
 */
class Quote
{
    /**
     * @var \Quote\Item[]
     */
    protected $items = [];

    /**
     * @var \Quote\Item[]
     */
    protected $total;

    /**
     * Quote constructor.
     */
    public function __construct()
    {
        $this->total = new \Quote\Total($this);
    }

    /**
     * @param \Product $product
     * @param int $qty
     * @return $this
     */
    public function add(\Product $product, $qty = 1)
    {
        $productCode = $product->getCode();

        if (!$productCode) {
            return;
        }

        /** @var \Item $item */
        $item = $this->getItemByCode($productCode);

        if ($item) {
            $item->setQty($item->getQty() + $qty);

        } else {
            $item = new \Quote\Item();
            $item->setProduct($product);
            $item->setQty($qty);
            $item->setPrice($product->getPrice());

            $this->items[] = $item;
        }

        $this->total->calculate();

        return $this;
    }

    /**
     * @return int
     */
    public function isEmpty()
    {
        return !$this->getItems() || !count($this->getItems());
    }

    /**
     * @param $productCode
     * @return \Quote\Item|null
     */
    protected function getItemByCode($productCode)
    {
        foreach ($this->items as $item) {
            if ($item->getProduct()->getCode() == $productCode) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @return \Quote\Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return \Quote\Item[]
     */
    public function getQty()
    {
        $qty = 0;
        foreach ($this->items as $item) {
            $qty += $item->getQty();
        }
        return $qty;
    }

    /**
     * @param $item
     * @return array
     */
    public function getItemTotals($item)
    {
        return $this->total->getItemTotals($item);
    }

    /**
     * @param $item
     * @return int|\Quote\Total\AbstractTotal
     */
    public function getItemAmount($item)
    {
        return $this->total->getItemAmount($item);
    }

    /**
     * @return float
     */
    public function getQuoteAmount()
    {
        return $this->total->getQuoteAmount($this);
    }

    /**
     * @param $item
     * @return array
     */
    public function getQuoteTotals()
    {
        return $this->total->getQuoteTotals($this);
    }
}
