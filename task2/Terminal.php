<?php

/**
 * Class Terminal
 */
class Terminal
{
    /**
     * @var \Quote
     */
    protected $quote;

    /**
     *
     */
    public function newCustomer()
    {
        $this->quote = null;
    }

    /**
     * @return \Quote
     */
    public function getQuote()
    {
        $this->quote = $this->quote ?: new \Quote();
        return $this->quote;
    }

    /**
     * @param string $line
     */
    public function scanLine($line)
    {
        $productCodes = str_split($line);
        foreach ($productCodes as $productCode) {
            $this->scanCode($productCode, 1);
        }
    }


    /**
     * @param string $line
     */
    public function getLine()
    {
        $line = '';
        /** @var \Quote\Item $item */
        foreach ($this->getQuote()->getItems() as $item) {
            $line .= str_pad('', $item->getQty(), $item->getProduct()->getCode());
        }
        return $line;
    }

    /**
     * @param string $productCode
     * @param int $qty
     */
    public function scanCode($productCode, $qty = 1)
    {
        if (!$productCode) {
            return null;
        }
        $product = new \Product();
        $product->loadByCode($productCode, $qty);

        $quote = $this->getQuote();
        $quote->add($product);
    }

    /**
     *
     */
    public function printCheck()
    {
        /** @var  $items */
        $quote = $this->getQuote();

        if ($quote->isEmpty()) {
            echo "No Line: Quote is empty, bye.";
            echo "\n\n\n";

        } else {
            echo "Line: {$this->getLine()}\n";

            $items = $quote->getItems();
            foreach ($items as $item) {
                $product = $item->getProduct();
                $productName = $item->getProduct()->getName();
                $itemAmount = $quote->getItemAmount($item);
                $itemTotals = $quote->getItemTotals($item);

                echo "\t`{$productName}` ({$product->getCode()}); qty {$item->getQty()}, price {$product->getPrice()}\n";
                foreach ($itemTotals as $itemTotal) {
                    if (isset($itemTotal['code'])) {
                        $info = $itemTotal['info'] ? '/' . $itemTotal['info'] . '/' : '';
                        echo "\t\t{$itemTotal['code']} {$itemTotal['total']}\$ {$info}\n";
                    }
                    echo "\t\tSub Total {$itemAmount}\$\n";
                }
            }
            echo "\t------\n";
            echo "\tTotals\n\t\tqty: {$quote->getQty()};\n";
            $quoteTotals = $quote->getQuoteTotals();
            foreach ($quoteTotals as $quoteTotal) {
                if (isset($quoteTotal['code'])) {
                    echo "\t\t{$quoteTotal['code']} {$quoteTotal['total']}\$;\n";
                }
            }
            echo "\t\tTotal Amount {$quote->getQuoteAmount()}$;\n";
            echo "\n\n\n";
        }
    }
}
