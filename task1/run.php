<?php
/**
 * Calculate Arith. progression from start to end with step 1
 * Limited by 2^31.
 * For work with numbers larger then 2^31 - need another approach and more time
 *
 * @param int $startN
 * @param int $endN
 * @return string
 */
function  sumAP($startN, $endN) {
    $cntN = $endN - $startN + 1;
    $cntN2 = intval($cntN / 2);

    /* cnt */
    if (!($cntN > 0) || $endN > 9223372036854775807) {
        //echo "Zerro or Negative";
        return 'NaN';
    }

    /* numbers */
    $aa = array_reverse(array_map('intval', str_split($startN)));
    $bb = array_reverse(array_map('intval', str_split($endN)));
    $tt = array_reverse(array_map('intval', str_split($cntN2)));
    $sum = [];

    $bbCnt = count($bb);
    $ttCnt = count($tt);
    $mcarry = 0;
    for ($j = 0; $j < $ttCnt; $j++) {
        /* sum = (a1 + a2) * (cnt / 2) */
        $scarry = 0;
        for ($i = 0; $i < $bbCnt + 1; $i++){
            /* vars */
            $a = isset($aa[$i]) ? $aa[$i] : 0;
            $b = isset($bb[$i]) ? $bb[$i] : 0;
            $t = $tt[$j];
            $c = isset($sum[$i + $j]) ? $sum[$i + $j] : 0;

            /* for odd count, /2 */
            $sodd = ($cntN % 2 && $i == 0);
            $modd = ($cntN % 2 && $j == $ttCnt - 1) ? $a : 0;

            /* s = a + b */
            $s = $sodd + $a + $b + $scarry;
            $scarry = intval($s / 10);
            $s = $s % 10;

            /* c = (s x n) */
            $m = $modd + $s * $t + $c + $mcarry;
            $sum[$i + $j] = $m % 10;
            $mcarry = intval($m / 10);
        }
    }

    /* remove first zerro */
    $sum[] = $mcarry;
    $sum = array_reverse($sum);
    foreach ($sum as $i => $s){
        if ($s) break;
        unset($sum[$i]);
    }

    return implode('', $sum);
}

echo '98 .. 99 = ' . sumAP(98, 99) . "\n";
echo '83 .. 98 = ' . sumAP(83, 98) . "\n";
echo '1 .. 5 = ' . sumAP(1, 5) . "\n";
echo '5 .. 5 = ' . sumAP(5, 5) . "\n";
echo '4 .. 5 = ' . sumAP(4, 5) . " \n";
echo '6 .. 5 = ' . sumAP(6, 5) . " \n";
echo '1 .. ' . pow(2, 31) . ' = ' . sumAP(1, pow(2, 31)) . " \n";
echo '1 .. ' . pow(2, 32) . ' = ' . sumAP(1, pow(2, 32)) . " \n";
echo '1 .. 9223372036854775807 = ' . sumAP(1, 9223372036854775807) . " \n";
echo '1 .. 9223372036854775808 = ' . sumAP(1, 19223372036854775810) . " \n";
