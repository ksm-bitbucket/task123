<?php
/**
 * Copyright © 2016 Born. All rights reserved.
 * See Born.txt for license details.
 */
namespace Born\OrderController\Api;

/**
 * Interface BornGuestOrderHistory
 * @package Born\OrderController\Api
 */
interface BornGuestOrderHistoryInterface
{
    /**
     * @return \Born\OrderController\Api\Data\OrderHistoryInterface $orderHistory
     */
    public function getOrderHistory();
}
