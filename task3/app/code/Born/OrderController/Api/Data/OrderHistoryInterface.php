<?php
/**
 * Copyright © 2016 Born. All rights reserved.
 * See Born.txt for license details.
 */
namespace Born\OrderController\Api\Data;

interface OrderHistoryInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */
    const ORDERS = 'orders';

    /**#@-*/
    /**
     * @return \Born\OrderController\Api\Data\OrderInterface[]
     */
    public function getOrders();

    /**
     * @param \Born\OrderController\Api\Data\OrderInterface[]
     * @return mixed
     */
    public function setOrders($data);

}
