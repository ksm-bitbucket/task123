<?php
/**
 * Copyright © 2016 Born. All rights reserved.
 * See Born.txt for license details.
 */
namespace Born\OrderController\Api\Data;

interface OrderInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */
    const ORDER_ID = 'order_id';
    const ORDER_GRAND_TOTAL = 'grand_total';
    const ORDER_STATUS = 'grand_total';
    const ORDER_PRODUCTS = 'products';

    /**#@-*/
    /**
     * @return int
     */
    public function getOrderId();

    /**
     * @param string $data
     * @return $this
     */
    public function setOrderId($data);

    /**
     * @return float
     */
    public function getGrandTotal();

    /**
     * @param string $data
     * @return $this
     */
    public function setGrandTotal($data);

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @param string $data
     * @return $this
     */
    public function setStatus($data);

    /**#@-*/
    /**
     * @return \Born\OrderController\Api\Data\ProductInterface[]
     */
    public function getProducts();

    /**
     * @param \Born\OrderController\Api\Data\ProductInterface[]
     * @return mixed
     */
    public function setProducts($data);
}
