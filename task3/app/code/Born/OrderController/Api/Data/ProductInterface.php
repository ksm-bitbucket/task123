<?php
/**
 * Copyright © 2016 Born. All rights reserved.
 * See Born.txt for license details.
 */
namespace Born\OrderController\Api\Data;

interface ProductInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */
    const PRODUCT_ID = 'product_id';
    const PRODUCT_SKU = 'sku';

    /**#@-*/
    /**
     * @return string
     */
    public function getProductId();

    /**
     * @param string $data
     * @return $this
     */
    public function setProductId($data);

    /**
     * @return string
     */
    public function getSku();

    /**
     * @param string $data
     * @return $this
     */
    public function setSku($data);
}
