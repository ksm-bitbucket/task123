<?php
/**
 * Copyright © 2016 Born. All rights reserved.
 * See Born.txt for license details.
 */
namespace Born\OrderController\Model\Api;

/**
 * Shipping method read service.
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class BornGuestOrderHistory implements \Born\OrderController\Api\BornGuestOrderHistoryInterface
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $salesOrderCollectionFactory;

    /**
     * @var \Born\OrderController\Model\Api\Data\OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \Born\OrderController\Model\Api\Data\OrderHistoryFactory
     */
    protected $orderHistoryFactory;

    /**
     * @var \Born\OrderController\Model\Api\Data\ProductFactory
     */
    protected $productFactory;

    /**
     * BornGuestOrderHistory constructor.
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $salesOrderCollectionFactory
     * @param Data\OrderFactory $orderFactory
     * @param Data\OrderHistoryFactory $orderHistoryFactory
     */
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $salesOrderCollectionFactory,
        \Born\OrderController\Model\Api\Data\OrderFactory $orderFactory,
        \Born\OrderController\Model\Api\Data\OrderHistoryFactory $orderHistoryFactory,
        \Born\OrderController\Model\Api\Data\ProductFactory $productFactory
    ) {
        $this->salesOrderCollectionFactory = $salesOrderCollectionFactory;
        $this->orderFactory = $orderFactory;
        $this->orderHistoryFactory = $orderHistoryFactory;
        $this->productFactory = $productFactory;
    }

    /**
     * @return \Born\OrderController\Api\Data\OrderHistory $orderHistory
     */
    public function getOrderHistory()
    {
        /** @var \Born\OrderController\Model\Api\Data\OrderHistory $history */
        /** @var \Magento\Sales\Model\Order $saleOrder */
        /** @var \Born\OrderController\Model\Api\Data\Order $order */
        /** @var \Magento\Sales\Model\Order\Item $item */
        /** @var \Born\OrderController\Model\Api\Data\Product $product */

        $history = $this->orderHistoryFactory->create();

        $salesOrderCollection = $this->salesOrderCollectionFactory->create();
        $salesOrderCollection->addAttributeToFilter('customer_is_guest', 1);

        foreach ($salesOrderCollection as $saleOrder){
            $order = $this->orderFactory->create();
            $order->setOrderId($saleOrder->getId());
            $order->setGrandTotal($saleOrder->getGrandTotal());
            $order->setStatus($saleOrder->getStatus());

            foreach ($saleOrder->getAllVisibleItems() as $item) {
                $product = $this->productFactory->create();
                $product->setProductId($item->getProductId());
                $product->setSku($item->getSku());

                $order->addProduct($product);
            }
            $history->addOrder($order);
        }

        return $history;
    }
}
