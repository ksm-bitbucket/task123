<?php
/**
 * Copyright © 2016 Born. All rights reserved.
 * See Born.txt for license details.
 */
namespace Born\OrderController\Model\Api\Data;

/**
 * @codeCoverageIgnoreStart
 */
class Product
    extends \Magento\Framework\Model\AbstractExtensibleModel
    implements \Born\OrderController\Api\Data\ProductInterface
{
    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * @param string $data
     * @return $this
     */
    public function setProductId($data)
    {
        return $this->setData(self::PRODUCT_ID, $data);
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->getData(self::PRODUCT_SKU);
    }

    /**
     * @param string $data
     * @return $this
     */
    public function setSku($data)
    {
        return $this->setData(self::PRODUCT_SKU, $data);
    }

}
