<?php
/**
 * Copyright © 2016 Born. All rights reserved.
 * See Born.txt for license details.
 */
namespace Born\OrderController\Model\Api\Data;

/**
 * @codeCoverageIgnoreStart
 */
class Order
    extends \Magento\Framework\Model\AbstractExtensibleModel
    implements \Born\OrderController\Api\Data\OrderInterface
{
    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * @param string $data
     * @return $this
     */
    public function setOrderId($data)
    {
        return $this->setData(self::ORDER_ID, $data);
    }

    /**
     * @return float
     */
    public function getGrandTotal()
    {
        return $this->getData(self::ORDER_GRAND_TOTAL);
    }

    /**
     * @param string $data
     * @return $this
     */
    public function setGrandTotal($data)
    {
        return $this->setData(self::ORDER_GRAND_TOTAL, $data);
    }

    /**
     * @return float
     */
    public function getStatus()
    {
        return $this->getData(self::ORDER_STATUS);
    }

    /**
     * @param string $data
     * @return $this
     */
    public function setStatus($data)
    {
        return $this->setData(self::ORDER_STATUS, $data);
    }

    /**
     * @return \Born\OrderController\Api\Data\OrderHistoryInterface[]
     */
    public function getProducts()
    {
        return $this->getData(self::ORDER_PRODUCTS);
    }

    /**
     * @param \Born\OrderController\Api\Data\OrderHistoryInterface[] $data
     * @return $this
     */
    public function setProducts($data)
    {
        return $this->setData(self::ORDER_PRODUCTS, $data);
    }

    /**
     * @param $order
     */
    public function addProduct($product)
    {
        $products = $this->getProducts();
        $products = $products ?: [];
        $products[] = $product;
        $this->setProducts($products);
    }

    /**
     * @param array $keys
     * @return array
     */
    public function toArray(array $keys = [])
    {
        $arr = parent::toArray($keys);
        if ($products = $this->getProducts()) {
            $arr['products'] = [];
            $products = is_array($products) ? $products : [$products];
            foreach ($products as $product) {
                $arr['products'][] = $product->toArray();
            }
        }
        return $arr;
    }
}
