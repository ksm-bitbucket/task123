<?php
/**
 * Copyright © 2016 Born. All rights reserved.
 * See Born.txt for license details.
 */
namespace Born\OrderController\Model\Api\Data;

/**
 * @codeCoverageIgnoreStart
 */
class OrderHistory
    extends \Magento\Framework\Model\AbstractExtensibleModel
    implements \Born\OrderController\Api\Data\OrderHistoryInterface
{
    /**
     * @return \Born\OrderController\Api\Data\OrderHistoryInterface[]
     */
    public function getOrders()
    {
        return $this->getData(self::ORDERS);
    }

    /**
     * @param \Born\OrderController\Api\Data\OrderHistoryInterface[] $data
     * @return $this
     */
    public function setOrders($data)
    {
        return $this->setData(self::ORDERS, $data);
    }

    /**
     * @param $order
     */
    public function addOrder($order)
    {
        $orderHistory = $this->getOrders();
        $orderHistory = $orderHistory ?: [];
        $orderHistory[] = $order;
        $this->setOrders($orderHistory);
    }

    /**
     * @param array $keys
     * @return array
     */
    public function toArray(array $keys = [])
    {
        $arr = parent::toArray($keys);
        $arr['orders'] = [];
        foreach ($this->getOrders() as $order) {
            $arr['orders'][] = $order->toArray();
        }
        return $arr;
    }
}
