<?php
/**
 * Copyright © 2015 Born. All rights reserved.
 * See Born.txt for license details.
 */
namespace Born\OrderController\Controller\History;

class Guest extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Born\OrderController\Model\Api\BornGuestOrderHistory
     */
    protected $bornGuestOrderHistory;

    /**
     * Guest constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Born\OrderController\Model\Api\BornGuestOrderHistory $bornGuestOrderHistory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Born\OrderController\Model\Api\BornGuestOrderHistory $bornGuestOrderHistory
    ) {
        parent::__construct($context);

        $this->resultJsonFactory = $resultJsonFactory;
        $this->bornGuestOrderHistory = $bornGuestOrderHistory;
    }


    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $orderHistory = $this->bornGuestOrderHistory;
        /** @var \Born\OrderController\Model\Api\Data\OrderHistory $history */
        $history = $orderHistory->getOrderHistory();

print_r($history->toJson());exit;
        return $this->resultJsonFactory->create()->setData($orderHistory->getOrderHistory()->getData());
    }
}
