<?php
/**
 * Copyright © 2015 Born. All rights reserved.
 * See Born.txt for license details.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Born_OrderController',
    __DIR__
);
